# POM - Parabola Official Multimedia (text data only)

It's official collections of multimedia files from Parabola project (https://parabola.nu).

Note:
      This source contains files with only text data,
      the source with contains files with binary data is:
      * "https://repo.parabola.nu/src/others/multimedia/pom"

The COPYING file in this directory contains attributions and licensing terms for this 
collection.
